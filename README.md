# Action Fulfillment Tutorial

## What this will teach you

This tutorial will walk you through creating a very simple app that listens for people discussing the need for a meeting, and offers them the opportunity to schedule one. It is designed to show you exactly how to begin leveraging Watson Work Services' "Action Fulfillment" API.

## High Level Overview

Action Fulfillment (hereafter referred to as AF), is a way to allow private interaction between a third party application and a user. This interaction takes place in a separate dialog box presented to the user who initiates the interaction.

As the conversation unfolds in a space, either an instance of Watson Conversation (hereafter referred to as WC) or the third party app itself watches for what are called **intents**. These are phrases that suggest the user wishes to perform an action. For example, the phrase "we should probably schedule a meeting" could be designated an intent.

Once an intent has been identified, either WC or the app sends a notification to Watson Work Services, which then annotates the message in question. That annotation looks like a purple underline of the message.

Any user who sees that can then click on the message, and trigger the interaction.

## Creating The App
*All code examples will be written in Javascript, specifically, Node,JS and Express, and will use WC for intent recognition.*

#### Setting up Watson Conversation

From the Bluemix Services dashboard, choose to create a new service, and then select "Watson" from the left hand side list. Select "Conversation" from the middle panel.

<img src="pictures/bluemix_watson.png"></img>

Accept all defaults, and click "Create" in the bottom right.

On the next page, before selecting to "Launch tool", take a moment to look at the "Service credentials", select to "View credentials", and copy down the username and password. You will need them later when connecting this WC instance to your application.

<img src="pictures/credentials.png"></img>

Now that you have those credentials, go ahead and go back to "Manage", and launch the tool. Select "create" on the next screen to create a new workspace to house your WC instance. Feel free to name it anything you like.

The next screen will be the **intents** manager. Select "create new", and give this intent the name "create_meeting". Next, click on the line to "add a user example". You will need to add 5 examples of what phrase might suggest the user wants to create a meeting. Let's use the following phrases:
* we should create a meeting
* a meeting would help
* let's have a meeting
* do we want a meeting
* schedule a meeting

At this point, your intent should look like:

<img src="pictures/intent_example.png"></img>

Now let's move over to the "Dialog" tab.

<img src="pictures/dialog.png"></img>

We need to create a conversation "node" that will do something in response to our intent being recognized.

Click on "Create". You are presented with one empty node. Name this node "Create a Meeting". Under the "trigger" section, click next to the "if" and enter a pound symbol. This will create a dropdown menu where you can select from any intents you have created. In our case, we see "#create_meeting" and "#(create new condition)". Select the "#create_meeting" intent.

<img src="pictures/dialog_node_example.png"></img>

Finally, let's add our response. You'll need to click the three vertical circles to the right and select "JSON". That will let us craft a custom response.

<img src="pictures/dialog_response_example1.png"></img>

 The JSON we will place there is this:
```JSON
{
  "output": {
    "actions": [
      "new_create_meeting_request"
    ]
  }
}
```
The value of "actions" will be what is returned to our app whenever WC recognizes the "create_meeting" intent.

Before we leave WC, there is one more piece of information we need - the conversation ID. Over on the left, click on the "hamburger" icon, and then "Back to workspaces".

<img src="pictures/back_to_workspace.png"></img>

At the top right of the box describing your workspace, click on the three vertical dots, and then select "View details".

<img src="pictures/wc_overview.png"></img>

Copy down the Workspace ID (Workspace ID is the same as conversation ID).

<img src="pictures/wc_id.png"></img>

#### Setting up our app

First order of business is to connect our instance of WC to our app. From the app dashboard, select "make it cognitive" from the left side menu. Using the credentials and conversation ID obtained earlier, complete the missing fields and click "Connect".

<img src="pictures/adding_WC_to_app.png"></img>

Now, we need to make sure our app listens for the right callback. Select "Listen to Events" from the left panel. Add a webhook, if you don't already have one, or just edit the existing one, but make sure "message-annotation-added" is selected.

The callback we want to listen for looks like this:
```python
{
  spaceName = ww-box dev,
  spaceId = 58fb7e28e4b0180503eccc07,
  annotationPayload = {
    "type":"actionSelected",
    "annotationId":null,
    "type":"actionSelected",
    "version":"1.0",
    "created":null,
    "createdBy":null,
    "updated":null,
    "updatedBy":null,
    "tokenClientId":null,
    "conversationId":"58fb7e28e4b0180503eccc07",
    "targetDialogId":"recognized_create_meeting_request",
    "referralMessageId":"5908e6d3e4b00c349778aa77",
    "actionId":"recognized_create_meeting_request",
    "targetUserId":"0bf536e2-0dcd-430a-b7fb-84740d15b522"
  },
  messageId=5908e9d7e4b07c188749c8d7,
  annotationType=actionSelected,
  annotationId=5908e9d7e4b07c188749c8d8,
  time=1493756375155,
  type=message-annotation-added,
  userName=Jeff Dean,
  userId=c7611fc0-8344-1034-8b06-875b21a6991e
}
```

In the app itself, let's add a handler for these new callbacks.

```Javascript
app.post("ww", function (request, response) {
  var body = request.body;
  var eventType = body.type;
  var annotationtype = body.annotationType;

  // omitted

  else if (annotationType === "actionSelected") {
    var payload = JSON.parse(body.annotationPayload);

    // New interaction?
    if (payload.actionId.includes("new")) {
      handleNewDialog(body, token, url);
    }
    // or response?
    else {
      handleDialogResponse(body, token, url);
    }
  }
}
```

So let's discuss what we have here. We look at annotationType to see if we have a "actionSelected" annotation - that's what we get when a user clicks on the underlined message. Unfortunately, we can't just stop there - we get the same exact annotation when a user has already started interacting with us. So we further have to decide if this is a new interaction, in which case the "actionId" will match the value of "action" in our WC dialog response, or some other value (which will tell us what the latest user action was in our interaction). Let's handle new interactions first.

```Javascript
var handleNewDialog = function (body, token, url) {
  var payload = JSON.parse(body.annotationPayload);
  var action = payload.actionId;
  if (action.includes("create_meeting_request")) {
    var buttons = [];
    buttons.push(createButton("Yes", "CREATE_MEETING", "PRIMARY"));
    buttons.push(createButton("No", "DO_NOT_CREATE_MEETING", "PRIMARY"));
    var dialog = createDialogText("Create a new Meeting",
      "Would you like to create a new meeting",
      buttons.join(','));
    sendResponse(dialog, body, token, url);
  }
}
```

Here we determine what intent was recognized, and we craft a response accordingly. We use "Create a new meeting" as our dialog title, and "Would you like to create a new meeting" as our description. We then add two buttons - yes and no. Before we look at how the buttons and dialog description and title are crafted, let's take a look at the form of our final response. This will be a call to the GraphQL endpoint, and the body will look like this:
```GraphQL
mutation {
    createTargetedMessage(input: {
      conversationId: "58fb7e28e4b0180503eccc07"
      targetUserId: "c7611fc0-8344-1034-8b06-875b21a6991e"
      targetDialogId: "new_create_meeting_request"
      annotations: [
        {
          genericAnnotation: {
            title: "Create a new meeting",
            text: "Would you like to create a new meeting?",
            color: "#016F4A",
            buttons: [
              {
                postbackButton: {
                  title: "Yes",
                  id: "CREATE_MEETING",
                  style: PRIMARY
                }
              },
              {
                postbackButton: {
                  title: "Public",
                  id: "DO_NOT_CREATE_MEETING",
                  style: PRIMARY
                }
              }
            ]
          }
        }
      ]
    }) {
      successful
    }
}
```
The quotes are both important and easy to inadvertently mess up. To ensure we get them right, we'll exploit the util.format method. Here's our method to create a button:
```Javascript
var createButton = function (label, id, style) {
  const button = util.format(`
    {
      postbackButton: {
        title: "%s",
        id: "%s",
        style: %s
      }
    }`, label, id, style);
  return button;
}
```
and our method to create the dialog:
```Javascript
var createDialogText = function (title, description, buttons) {
  winston.log("debug", "Entered action.createDialogText");
  var color = "#016F4A";
  const annotation = util.format(`
    {
      genericAnnotation: {
        title: "%s",
        text: "%s",
        color: "%s",
        buttons: [ %s ]
      }
    }`, title, description, color, buttons);
  return annotation;
}
```

Finally, we can send off our completed mutation to the GraphQL endpoint to present the user with our dialog:
```Javascript
var sendResponse = function (dialog, body, token, url) {
  var payload = JSON.parse(body.annotationPayload);
  var conversation_id = payload.conversationId;
  var target_user_id = body.userId;
  var target_dialog_id = payload.targetDialogId;
  var view = "PUBLIC, BETA"
  var mutation_body = util.format(`
    mutation {
      createTargetedMessage(input: {
        conversationId: "%s"
        targetUserId: "%s"
        targetDialogId: "%s"
        annotations: [ %s ]
      }) {
        successful
      }
    }`, conversation_id, target_user_id, target_dialog_id, dialog);
  ww.makeGraphQLCall(mutation_body, view, token, url, function (err, res) {
    if (err) {
      winston.log("error", "mutation graphql call failed", err);
    } else {
      winston.log("debug", "mutation graphql call succeeded", res);
    }
  });
}
```

Ok, so we have presented the first screen to the user - what now? Well, we need to add a handler for when they respond back to us. Remember when we crafted our buttons, we added a field called "id". For the "yes" button it was "CREATE_MEETING", and for no it was "DO_NOT_CREATE_MEETING". Once the user has selected an option, either yes or no, we'll get an identical "actionSelected" annotation callback, but this time, the "actionId" will not be "new_create_meeting_request" - it will either be "CREATE_MEETING" or "DO_NOT_CREATE_MEETING".

This is how we will know what the user chose to do. Here's our handler:
```Javascript
var handleDialogResponse = function (body, token, url) {
  var payload = JSON.parse(body.annotationPayload);
  var action = payload.actionId;
  if (action.includes("CREATE_MEETING")) {
    var dialog = createDialogText("Create a new meeting",
      "I have scheduled  a new meeting",
      "");
    sendResponse(dialog, body, token, url);
  }
  else if (action.includes("DO_NOT_CREATE_MEETING")) {
    var dialog = createDialogText("Create a new meeting",
      "No meeting has been scheduled.",
      "");
    sendResponse(dialog, body, token, url);
  }
}
```
Note that we did not create any buttons here, although we could have, if further interaction was required.

## References

To see the entire example app, please visit https://bitbucket.org/jared-wallace/example-action-fulfillment

For complete documentation on Action-Fulfillment, see
https://workspace.ibm.com/developer/docs

To contact the author, email jared-wallace@us.ibm.com
